using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class OpenIndice : MonoBehaviour
{
    public GameObject vis1;
    public GameObject vis2;
    public GameObject vis3;
    public GameObject vis4;
    public Animator anim;

    void Update()
    {
        if (vis1.GetComponent<Rigidbody>().useGravity == true && vis2.GetComponent<Rigidbody>().useGravity == true && vis3.GetComponent<Rigidbody>().useGravity == true && vis4.GetComponent<Rigidbody>().useGravity == true)
        {
            anim.enabled = false;
            anim.Play("anim12");
            //gameObject.GetComponent<Rigidbody>().useGravity = true;   
        }
    }
}
