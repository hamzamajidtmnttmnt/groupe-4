using UnityEngine;
    using UnityEngine.AI;
    
    public class GuardMove : MonoBehaviour {
       
       private int _currentPoint = 0;
       private bool _reversePath;
       NavMeshAgent _agent;
       [SerializeField] private Transform[] _waypoints;
       private Animator _anim;
       bool startLook =false;
       
       void Start () {
          _agent = GetComponent<NavMeshAgent>();
          _anim = GetComponent<Animator>();
       }

        void Update(){
            CalculateAIMovement();
        }

        void CalculateAIMovement(){
                        _agent.destination=_waypoints[_currentPoint].position;
            if (_anim.GetCurrentAnimatorStateInfo(0).IsName("LookAround") && !startLook)
            {
                pause();
                startLook=true;
            } else if (startLook)
            {
                resume();
                startLook=false;
            }

            if (_agent.remainingDistance < 0.25f && _agent.remainingDistance>0)
            {
                _anim.SetBool("stop",true);

                if (!_reversePath)
                {
                    MoveForward();
                }
                else if(_reversePath){
                    MoveReverse();
                }

            }
            else{
                _anim.SetBool("stop",false);
            }
            _agent.destination=_waypoints[_currentPoint].position;
        }

Vector3 lastAgentVelocity;
NavMeshPath lastAgentPath;
float acc;
float speed;
float ang;

 void pause() {
         lastAgentVelocity = _agent.velocity;
         lastAgentPath = _agent.path;
         acc=_agent.acceleration;
         speed=_agent.speed;
         ang=_agent.angularSpeed;
         _agent.velocity = Vector3.zero;
         _agent.acceleration=0;
         _agent.speed=0;
         _agent.angularSpeed=0;
         _agent.ResetPath();
     }
     
     void resume() {
         _agent.velocity = lastAgentVelocity;
        _agent.acceleration=acc;
         _agent.speed=speed;
         _agent.angularSpeed=ang;
         _agent.SetPath(lastAgentPath);
     }

        void MoveForward(){
            _currentPoint ++;
            if (_currentPoint>=_waypoints.Length-1)
            {
                _reversePath =true;
            }
        }

        void MoveReverse(){
            _currentPoint--;
            if (_currentPoint<=0)
            {
                _reversePath = false;
            }
        }

    }
