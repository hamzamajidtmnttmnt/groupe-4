using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class insertDialogue : MonoBehaviour
{
    public GameObject canvasDialogue;
    private bool open;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        canvasDialogue.SetActive(false);
        open = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(OVRInput.GetDown(OVRInput.Button.One) && !open && (player.transform.position - this.transform.position).magnitude < 30f)
        {
            canvasDialogue.SetActive(true);
        }
        if (OVRInput.GetDown(OVRInput.Button.One) && (player.transform.position-this.transform.position).magnitude>=30f)
        {
            canvasDialogue.SetActive(false);
        }
    }
}
